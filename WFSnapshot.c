#include <stdlib.h>

struct StampedSnap
{
    long    stamp;
    int     value;
    int*    snap;
};

struct WFSnapshot
{
    struct StampedSnap* values[2];
    int* valueindex;
    int nwriter;
};

struct WFSnapshot* wf_init(int nwriter)
{
    struct WFSnapshot* seq;

    seq = (struct WFSnapshot*) malloc(sizeof(struct WFSnapshot));
    seq->values[0] = (struct StampedSnap*) malloc(nwriter * sizeof(struct StampedSnap));
    seq->values[1] = (struct StampedSnap*) malloc(nwriter * sizeof(struct StampedSnap));
    seq->valueindex = (int*) malloc(nwriter * sizeof(int));

    int i, j;
    for(i=0; i<nwriter; i++)
    {
        seq->values[0][i].stamp = 0;
        seq->values[0][i].value = 0;
        seq->values[0][i].snap = (int*) malloc(nwriter * sizeof(int));

        seq->values[1][i].stamp = 0;
        seq->values[1][i].value = 0;
        seq->values[1][i].snap = (int*) malloc(nwriter * sizeof(int));
        for(j=0; j<nwriter; j++)
        {
            seq->values[0][i].snap[j] = 0;
            seq->values[1][i].snap[j] = 0;
        }

        seq->valueindex[i] = 0;
    }

    seq->nwriter = nwriter;

    return seq;
}

struct StampedSnap* wf_collect(struct WFSnapshot* seq, struct StampedSnap* snap)
{
    int i, j;
    struct StampedSnap instantSnap;

    for(i = 0; i < seq->nwriter; i++)
    {
        instantSnap = seq->values[seq->valueindex[i]][i];
        snap[i].stamp = instantSnap.stamp;
        snap[i].value = instantSnap.value;
        for(j = 0; j < seq->nwriter; j++)
        {
            snap[i].snap[j] = instantSnap.snap[j];
        }
    }
    return snap;
}

int* wf_scan(struct WFSnapshot* seq, int* result, int size)
{
    int i, j;

    struct StampedSnap old1[size];
    struct StampedSnap* oldsnap = old1;
    int snap1[size][size];
    for(i=0;i<size;i++)
    {
        oldsnap[i].snap = snap1[i];
    }
    struct StampedSnap new1[size];
    struct StampedSnap* newsnap = new1;
    int snap2[size][size];
    for(i=0;i<size;i++)
    {
        newsnap[i].snap = snap2[i];
    }
    int moved[size];
    for(i=0; i<size;i++)
    {
        moved[i] = 0;
    }

    wf_collect(seq, oldsnap);

    while(1)
    {
        wf_collect(seq, newsnap);

        int check = 0;
        for(i = 0; i < size; i++)
        {
            if(oldsnap[i].stamp != newsnap[i].stamp)
            {
                if(moved[i])
                {
                    for(j=0; j<size; j++)   result[j] = newsnap[i].snap[j];
                    return result;
                }
                else
                {
                    moved[i] = 1;
                    struct StampedSnap* temp = oldsnap;
                    oldsnap = newsnap;
                    newsnap = temp;
                    check = 1;
                    break;
                }
            }
        }
        if(!check)  break;
    }
    for(j=0; j<size; j++)   result[j] = newsnap[j].value;
    return result;
    
}

void wf_update(struct WFSnapshot* seq, int windex, int value)
{
    int i;
    int oldindex = (seq->valueindex[windex])%2;
    int newindex = (seq->valueindex[windex]+1)%2;

    wf_scan(seq, seq->values[newindex][windex].snap, seq->nwriter);
    seq->values[newindex][windex].stamp = seq->values[oldindex][windex].stamp+1;
    seq->values[newindex][windex].value = value;

    seq->valueindex[windex] = newindex;
}

void wf_free(struct WFSnapshot* seq)
{
    int i;
    for(i = 0; i < seq->nwriter; i++)
    {
        free(seq->values[0][i].snap);
        free(seq->values[1][i].snap);
    }
    free(seq->values[1]);
    free(seq->values[0]);
    free(seq);
}
