#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

struct SeqSnapshot* seq_init(int);
void seq_update(struct SeqSnapshot*, int, int);
int* seq_scan(struct SeqSnapshot*, int*, int);
void seq_free(struct SeqSnapshot*);
struct WFSnapshot* wf_init(int);
void wf_update(struct WFSnapshot*, int, int);
int* wf_scan(struct WFSnapshot*, int*, int);
void wf_free(struct WFSnapshot*);

int timeup;
long seqrcount[100];
long seqwcount[100];
long wfrcount[100];
long wfwcount[100];

//////////////////////////napshot without lock!/////////////////////////////
//reader
struct wf_rarg {
    struct WFSnapshot* seq;
    long rcount;
    int nwriter;
};

void* wf_reader(void* ptr) {
    struct wf_rarg* arg = (struct wf_rarg*) ptr;
    int i;

    int values[arg->nwriter];
    while(!timeup)
    {
        wf_scan(arg->seq, values, arg->nwriter);
        arg->rcount++;
//        printf("Reader Scanned %d times: ", ++arg->rcount);
//        for(i = 0; i < arg->nwriter; i++)   printf("%d ", values[i]);
//        printf("\n");
    }
}

//writer
struct wf_warg {
    struct WFSnapshot* seq;
    int wcount;
    int index;
};

void* wf_writer(void* ptr) {
    struct wf_warg* arg = (struct wf_warg*) ptr;
    int i=0;

    while(!timeup)
    {
        wf_update(arg->seq, arg->index, ++i);
        arg->wcount++;
//        printf("Writer %d Updated %d times\n", arg->index, arg->wcount);
    }
}

//test
void test_snapshot_without_lock()
{
    struct WFSnapshot* seq;
    int nwriter, i;

    //test with 1 writer to 100 writers
    for(nwriter = 1; nwriter <= 100; nwriter++)
    {
        //create//
        seq = wf_init(nwriter);        //initialize SeqSnapshot
        timeup = 0;

        pthread_t rthread;              //reader thread
        struct wf_rarg rarg;

        rarg.seq = seq;
        rarg.rcount = 0;
        rarg.nwriter = nwriter;
        pthread_create( &rthread, NULL, wf_reader, (void*) &rarg);

        pthread_t wthread[nwriter];     //writer thread
        struct wf_warg warg[nwriter];
        
        for(i = 0; i < nwriter; i++)
        {
            warg[i].seq = seq;
            warg[i].wcount = 0;
            warg[i].index = i;
            pthread_create( &(wthread[i]), NULL, wf_writer, (void*) &(warg[i]) );
        }

        //sleep//
        sleep(60);
        timeup = 1;
        pthread_join( rthread, NULL );
        for(i=0; i < nwriter; i++)
            pthread_join( wthread[i], NULL );

        //count//
        long total = 0;
        for(i=0; i < nwriter; i++)
            total+=warg[i].wcount;
        printf("%3d Writer Threads: Reader Scanned %ld, and Writer Updated %ld\n", nwriter, rarg.rcount, total);
        wfrcount[nwriter-1] = rarg.rcount;
        wfwcount[nwriter-1] = total;
        wf_free(seq);
    }
}

////////////////////////////snapshot with lock!///////////////////////////////
//reader
struct seq_rarg {
    struct SeqSnapshot* seq;
    long rcount;
    int nwriter;
};

void* seq_reader(void* ptr) {
    struct seq_rarg* arg = (struct seq_rarg*) ptr;
    int i;

    int values[arg->nwriter];
    while(!timeup)
    {
        seq_scan(arg->seq, values, arg->nwriter);
        arg->rcount++;
//        printf("Reader Scanned %d times: ", ++arg->rcount);
//        for(i = 0; i < arg->nwriter; i++)   printf("%d ", values[i]);
//        printf("\n");
    }
}

//writer
struct seq_warg {
    struct SeqSnapshot* seq;
    long wcount;
    int index;
};

void* seq_writer(void* ptr) {
    struct seq_warg* arg = (struct seq_warg*) ptr;
    int i=0;

    while(!timeup)
    {
        seq_update(arg->seq, arg->index, ++i);
        arg->wcount++;
//        printf("Writer %d Updated %d times\n", arg->index, arg->wcount);
    }
}

//test
void test_snapshot_with_lock()
{
    struct SeqSnapshot* seq;
    int nwriter, i;

    //test with 1 writer to 100 writers
    for(nwriter = 1; nwriter <= 100; nwriter++)
    {
        //create//
        seq = seq_init(nwriter);        //initialize SeqSnapshot
        timeup = 0;

        pthread_t rthread;              //reader thread
        struct seq_rarg rarg;

        rarg.seq = seq;
        rarg.rcount = 0;
        rarg.nwriter = nwriter;
        pthread_create( &rthread, NULL, seq_reader, (void*) &rarg);

        pthread_t wthread[nwriter];     //writer thread
        struct seq_warg warg[nwriter];
        
        for(i = 0; i < nwriter; i++)
        {
            warg[i].seq = seq;
            warg[i].wcount = 0;
            warg[i].index = i;
            pthread_create( &(wthread[i]), NULL, seq_writer, (void*) &(warg[i]) );
        }

        //sleep//
        sleep(60);
        timeup = 1;
        pthread_join( rthread, NULL );
        for(i=0; i < nwriter; i++)
            pthread_join( wthread[i], NULL );

        //count//
        long total = 0;
        for(i=0; i < nwriter; i++)
            total+=warg[i].wcount;
        printf("%3d Writer Threads: Reader Scanned %ld, and Writer Updated %ld\n", nwriter, rarg.rcount, total);
        seqrcount[nwriter-1] = rarg.rcount;
        seqwcount[nwriter-1] = total;
        seq_free(seq);
    }
}

/////////////////////////analysis!/////////////////////////
void analyze()
{
    int i;
    printf("\nWait Free Snapshot # of scan by read\n");
    for(i=0; i<100; i++)    printf("%ld\n", wfrcount[i]);
    printf("\nWait Free Snapshot # of update by write\n");
    for(i=0; i<100; i++)    printf("%ld\n", wfwcount[i]);
    printf("\nWait Free Snapshot # of scan by both read and write\n");
    for(i=0; i<100; i++)    printf("%ld\n", (wfrcount[i] + wfwcount[i]));

    printf("\n----------------------------------------------------\n");

    printf("\nSequential Snapshot # of scan by read\n");
    for(i=0; i<100; i++)    printf("%ld\n", seqrcount[i]);
    printf("\nSequential Snapshot # of update by write\n");
    for(i=0; i<100; i++)    printf("%ld\n", seqwcount[i]);

}

/////////////////////////////main!/////////////////////////////////
int main()
{
    printf("test without lock ( WF snapshot )\n");
    test_snapshot_without_lock();
    printf("\ntest with lock ( Seq snapshot )\n");
    test_snapshot_with_lock();
    printf("\nAnalysis\n");
    analyze();
}
