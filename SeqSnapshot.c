#include <stdlib.h>
#include <pthread.h>

struct SeqSnapshot
{
    int* values;
    pthread_mutex_t mutex;
};

struct SeqSnapshot* seq_init(int nwriter)
{
    struct SeqSnapshot* seq;
    
    seq = (struct SeqSnapshot*) malloc(sizeof(struct SeqSnapshot));
    seq->values = (int*) malloc(nwriter * sizeof(int));
    int i;
    for(i=0; i<nwriter; i++)    seq->values[i] = 0;
    pthread_mutex_init(&seq->mutex, NULL);

    return seq;
}

void seq_update(struct SeqSnapshot* seq, int windex, int value)
{
    pthread_mutex_lock(&seq->mutex);

    seq->values[windex] = value;

    pthread_mutex_unlock(&seq->mutex);
}

int* seq_scan(struct SeqSnapshot* seq, int* result, int size)
{
    int i;
    pthread_mutex_lock(&seq->mutex);

    for(i=0; i < size; i++)
        result[i] = seq->values[i];

    pthread_mutex_unlock(&seq->mutex);

    return result;
}

void seq_free(struct SeqSnapshot* seq)
{
    free(seq->values);
    free(seq);
}
